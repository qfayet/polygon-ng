import { Complex as Complex } from "./complex";
import { Base } from "./cycle";
import { Matrix as Mat, Matrix } from "./matrix";
import { computeDivisors, Rational } from "./rational";
import { Vector as Vec, Vector } from "./vector";


export abstract class Solver {

  constructor(public size: number, public divs: number[], public Rz: Complex[] | undefined) { }

  get p(): number {
    return this.getSize();
  }

  get q(): number {
    return this.getDiv();
  }

  abstract get n(): number;

  getSize(): number {
    return this.size;
  }

  getDivs(): number[] {
    return this.divs;
  }

  getDiv(): number {
    let divs = this.getDivs();
    return divs.length > 0 ? divs[divs.length - 1] : 1;
  }

  solve(): Complex[] {
    return this.Rz as Complex[];
  }

  checkRoots(): void {
    if (this.Rz === undefined) {
      this.Rz = this.solve();
    }
  }

  getRoots(): Complex[] {
    if (this.Rz === undefined) {
      this.Rz = this.solve();
    }
    return this.Rz as Complex[];
  }

  print(debug = false): void {
    this.checkRoots();
    this.printIntro(debug);
    this.printBody(debug);
    this.printOutro(debug);
  }

  printIntro(debug = false): void {
    console.log(`p: ${this.getSize()}`);
  }

  printBody(debug = false): void {
  }

  getEdges(): string[] {
    return this.getRoots().map(z => z.toStringPolar(this.getSize()));
  }

  getRootMin(): Complex {
    // return this.getRoots().sort(Complex.compare)[0];
    return this.getRoots().reduce((prev, curr) => Complex.compare(prev, curr) < 0 ? prev : curr);
  }

  getAccuracy(Rmin: Complex | undefined = undefined): number {
    if (!Rmin) Rmin = this.getRootMin();
    let diff = Math.abs(this.getSize() * Rmin.angle() / (2 * Math.PI) - 1);
    return Math.round(Math.log10(diff));
  }

  printOutro(debug = false): void {
    console.log(`Edges: ${this.getEdges().join(", ")}`);
    console.log(`Accuracy: >>> 1e${this.getAccuracy()} <<<`);
  }

  abstract decompose(q: number): Solver;

}


export abstract class SolverPrime extends Solver {

  R: number[][];

  constructor(p: number, divs: number[], public base: number[][], Rz: Complex[] | undefined = undefined) {
    super(p, divs, Rz);
    this.R = base.map(cycle => Vec.fromCycle(this.getSize(), cycle));
  }

  get n(): number {
    return this.getR().length;
  }

  getR(): number[][] {
    return this.R;
  }

  decompose(q: number): Solver {
    return new SolverPrimeChild(this, q);
  }

  getDim(): number {
    return this.base.length;
  }

  printIntro(debug = false): void {
    super.printIntro(debug);
    console.log(`Cycles: ${Base.toString(this.base)}`);
  }

  printOutro(debug = false): void {
    if (this.getDim() === this.getSize() - 1) {
      console.log("--------------------");
      super.printOutro(debug);
    }
  }

  locateV(v: number[]): number[] {
    return this.base.map(c => v[c[0]]);
  }

  locateM(m: number[][]): number[][] {
    return m.map(v => this.locateV(v));
  }

  computeV(v: number[]): Complex {
    return v.reduce((total, x, i) => total.sum(this.getRoots()[i].scale(x)), new Complex(0));
  }

  computeM(m: number[][], T: Complex[]): Complex {
    return m.reduce((total, v, i) => total.sum(this.computeV(v).mult(T[i])), new Complex(0));
  }

  getRExprTex(i: number, cycle: number[]): string {
    let thetas = cycle.map(c => `\\theta^{\\frac{${c}}{${this.getSize()}}}`);
    return `R_{${i}} = ${thetas.join(" + ")}`;
  }

  _RExprTexs: string[];
  getRExprTexs(): string[] {
    if (!this._RExprTexs) {
      this._RExprTexs = this.base.map((cycle, i) => this.getRExprTex(i, cycle));
    }
    return this._RExprTexs;
  }

  _RVecTexs: string[];
  getRVecTexs(): string[] {
    if (!this._RVecTexs) {
      this._RVecTexs = this.getR().map((v, i) => `R_{${i}} = ${Vector.toTex(v)}`);
    }
    return this._RVecTexs;
  }

}

export class SolverPrimeRoot extends SolverPrime {

  constructor(p: number) {
    super(p, [], Base.create(p), [new Complex(-1)]);
  }

}

export class SolverPrimeChild extends SolverPrime {

  Q: number[][][] | undefined;
  P: number[][][] | undefined;
  Pr: number[][][] | undefined;
  Pz: Complex[] | undefined;
  Qz: Complex[] | undefined;

  constructor(public parent: SolverPrime, q: number) {
    super(
      parent.getSize(),
      [...parent.getDivs(), q],
      Base.decomposeBase(parent.base, q)
    );
    this.init();
  }

  init(): void {
    let p = this.getSize();
    let n = this.getDim();
    let q = this.getDiv();
    this.Q = new Array(n);
    for (let i = 0; i < n; i++) {
      let off = Math.floor(i / q) * q;
      this.Q[i] = Mat.create(q, p);
      for (let j = 0; j < q; j++) {
        let cycle = this.base[off + j]
        for (let k = 0; k < cycle.length; k++) {
          this.Q[i][(i * j) % q][cycle[k]] = 1;
        }
      }
    }
  }

  getQ(): number[][][] {
    if (this.Q === undefined) {
      this.init();
    }
    return this.Q as number[][][];
  }

  build(): void {
    let p = this.getSize();
    let n = this.getDim();
    let q = this.getDiv();
    let Q = this.getQ();
    let S1 = Q[n - 1];
    let S = new Array<number[][]>(q);
    S[0] = Mat.one(S1);
    for (let i = 1; i < q; i++) {
      S[i] = Mat.mult(S[i - 1], S1);
    }
    this.P = new Array(n);
    for (let i = 0; i < n; i++) {
      this.P[i] = Mat.mult(Q[i], S[i % q]);
      if (i % q === q - 1) {
        this.P[i] = Mat.normalize(this.P[i], p, this.P[0][0]);
      } else {
        this.P[i] = Mat.simplify(this.P[i]);
      }
    }
    this.Pr = new Array(n);
    for (let i = 0; i < n; i++) {
      this.Pr[i] = this.parent.locateM(this.P[i]);
    }
  }

  getP(): number[][][] {
    if (this.P === undefined) {
      this.build();
    }
    return this.P as number[][][];
  }

  getPr(): number[][][] {
    if (this.Pr === undefined) {
      this.build();
    }
    return this.Pr as number[][][];
  }

  getPz(): Complex[] {
    if (this.Pz === undefined) {
      this.compute();
    }
    return this.Pz as Complex[];
  }

  getQz(): Complex[] {
    if (this.Qz === undefined) {
      this.compute();
    }
    return this.Qz as Complex[];
  }

  compute(): void {
    let n = this.getDim();
    let q = this.getDiv();
    let Pr = this.getPr();
    let Tz = new Array<Complex>(q);
    for (let i = 0; i < q; i++) {
      Tz[i] = Complex.theta(i, q);
    }
    this.Pz = new Array(n);
    for (let i = 0; i < n; i++) {
      this.Pz[i] = this.parent.computeM(Pr[i], Tz);
    }
    let Sz1 = this.Pz[n - 1].pow(1 / q);
    let Sz = new Array<Complex>(q);
    Sz[0] = new Complex(1);
    for (let i = 1; i < q; i++) {
      Sz[i] = Sz[i - 1].mult(Sz1);
    }
    this.Qz = new Array(n);
    for (let i = 0; i < n; i++) {
      this.Qz[i] = this.Pz[i].div(Sz[i % q]);
    }
    this.Rz = new Array(n);
    for (let i = 0; i < n; i++) {
      let off = Math.floor(i / q) * q;
      this.Rz[i] = new Complex(0);
      for (let j = 0; j < q; j++) {
        this.Rz[i] = this.Rz[i].sum(this.Qz[off + j].mult(Tz[(i * (q - j)) % q]));
      }
      this.Rz[i] = this.Rz[i].scale(1 / q);
    }
  }

  solve(): Complex[] {
    if (this.Rz === undefined) {
      this.compute();
    }
    return super.solve();
  }

  printBody(debug = false): void {
    let Rz = this.getRoots();
    let Pr = this.getPr();
    let Pz = this.Pz as Complex[];
    if (debug) {
      let Q = this.getQ();
      let P = this.P as number[][][];
      console.log("--------------------");
      Q.forEach((m, i) => {
        console.log(`Q[${i}]:`);
        Mat.print(m);
      })
      console.log("--------------------");
      P.forEach((m, i) => {
        console.log(`P[${i}]:`);
        Mat.print(m);
      })
    }
    console.log("--------------------");
    Pr.forEach((m, i) => {
      console.log(`P[${i}]:`);
      Mat.print(m);
    })
    console.log("--------------------");
    Pz.forEach((z, i) => {
      console.log(`P[${i}]: ${z.toString()}`);
    })
    console.log("--------------------");
    Rz.forEach((z, i) => {
      console.log(`R[${i}]: ${z.toString()}`);
    })
  }

  getQExprTex(i: number): string {
    let q = this.getDiv();
    let off = Math.floor(i / q) * q;
    let exprs = new Array<string>(q);
    for (let j = 0; j < q; j++) {
      exprs[j] = `R_{${off + j}} \\cdot \\theta^{\\frac{${(i * j) % q}}{${q}}}`
    }
    return `Q_{${i}} = ${exprs.join(" + ")}`;
  }

  _QExprTexs: string[];
  getQExprTexs(): string[] {
    if (!this._QExprTexs) {
      this._QExprTexs = this.Q.map((v, i) => this.getQExprTex(i));
    }
    return this._QExprTexs;
  }

  _QMatTexs: string[];
  getQMatTexs(): string[] {
    if (!this._QMatTexs) {
      this._QMatTexs = this.getQ().map((m, i) => `Q_{${i}} = ${Matrix.toTex(m)}`);
    }
    return this._QMatTexs;
  }

  getPExprTex(i: number): string {
    return `P_{${i}} = Q_{${i}} \\cdot Q_{${this.n - 1}}^{${i % this.q}}`;
  }

  getPMatTex(m: number[][], i: number): string {
    let coords = Matrix.toTex(this.getPr()[i], 'b');
    return `${this.getPExprTex(i)} = ${Matrix.toTex(m)} = ${coords}`;
  }

  _PMatTexs: string[];
  getPMatTexs(): string[] {
    if (!this._PMatTexs) {
      this._PMatTexs = this.getP().map((m, i) => this.getPMatTex(m, i));
    }
    return this._PMatTexs;
  }

  getPrPart(coef: number, i: number, start: boolean): string {
    let num = Math.abs(coef);
    if (num === 0) return "";
    let numPart = num === 1 ? i === 0 ? "1" : "" : `${num}`;
    let numAndX = i === 0 ? numPart : `${numPart} {R'}_{${i - 1}}`;
    if (coef < 0) return ` - ${numAndX}`;
    return start ? numAndX : ` + ${numAndX}`;
  }

  getPrRow(v: number[], degree: number, q: number): string {
    let parts: string[] = [];
    v.forEach((x, i) => {
      let part = this.getPrPart(x, i, parts.length === 0);
      if (part) parts.push(part);
    });
    if (parts.length === 0) return "";
    let expr = parts.join("");
    if (degree === 0) return expr;
    let theta = `\\theta^{\\frac{${degree}}{${q}}}`;
    if (parts.length === 1) return `${expr} \\cdot ${theta}`
    return `(${expr}) \\cdot ${theta}`;
  }

  getPrRows(m: number[][]): string {
    let r = "";
    for (let i = 0; i < m.length; i++) {
      let part = this.getPrRow(m[i], i, m.length);
      if (!part) continue;
      r += (!r || part.trim().startsWith("-")) ? part : ` + ${part}`;
    }
    return r;
  }

  getPCoordTex(m: number[][], i: number, numerical = false): string {
    let rExpr =  this.getPrRows(Matrix.simplifyCoords(m));
    let prefix = `P_{${i}} = \\: ${rExpr}`;
    return numerical && (rExpr.includes("theta") || rExpr.includes("R'")) ? `${prefix} \\; ${this.getPz()[i].toTexEq()}` : prefix;
  }

  getPCoordTexs(numerical = false): string[] {
    return this.getPr().map((m, i) => this.getPCoordTex(m, i, numerical));
  }

  getQnExpr(numerical = false): string {
    let prefix = `Q = Q_{${this.n - 1}} = \\sqrt[${this.q}]{P_{${this.n - 1}}}`;
    return numerical ? `${prefix} \\; ${this.getQz()[this.n - 1].toTexEq()}` : prefix;
  }

  getQResolvTex(z: Complex, i: number, numerical = false): string {
    let prefix = `Q_{${i}} = \\frac{P_{${i}}}{Q_{{${this.n - 1}}}^{{${i % this.q}}}}`;
    return numerical ? `${prefix} \\; ${z.toTexEq()}` : prefix;
  }

  getQResolvTexs(numerical = false): string[] {
    return this.getQz().map((z, i) => this.getQResolvTex(z, i, numerical));
  }

  getRResolvTex(z: Complex, i: number, numerical = false): string {
    let q = this.q;
    let off = Math.floor(i / q) * q;
    let sumExprs = new Array<string>(q);
    for (let j = 0; j < q; j++) {
      sumExprs[j] = `\\frac{Q_{${off + j}}}{\\theta^\\frac{${(i * j) % q}}{${q}}}`;
    }
    let prefix = `R_{${i}} = \\frac{1}{${q}} \\cdot (${sumExprs.join(" + ")})`
    return numerical ? `${prefix} \\; ${z.toTexEq()}` : prefix
  }

  getRResolvTexs(numerical = false): string[] {
    return this.getRoots().map((z, i) => this.getRResolvTex(z, i, numerical));
  }

  getSignExpr(j: number, exp: number, q: number): string {
    if (j === 0) return "";
    if (q === 2 && exp === 1) return " - ";
    return " + ";
  }

  getPQExpr(off: number, j: number): string {
    let pExpr = `P_{${off + j}}`;
    if (j === 0) return pExpr;
    let qExpr = j == 1 ? "Q" : `Q^{${j}}`;
    return `\\frac{${pExpr}}{${qExpr}}`;
  }

  getRThetaExpr(exp: number, q: number): string {
    if (exp === 0 || q === 2) return "";
    return ` \\cdot \\theta^{\\frac{${exp}}{${q}}}`;
  }

  getRExprAt(i: number, j: number, q: number): string {
    let off = Math.floor(i / q) * q;
    let exp = (i * (q - j)) % q;
    let signExpr = this.getSignExpr(j, exp, q);
    let pqExpr = this.getPQExpr(off, j);
    let thetaExpr = this.getRThetaExpr(exp, q);
    return signExpr + pqExpr + thetaExpr;
  }

  getRExpr2Tex(z: Complex, i: number, numerical = false): string {
    let q = this.q;
    let sumExpr = "";
    for (let j = 0; j < q; j++) {
      sumExpr += this.getRExprAt(i, j, q);
    }
    let prefix = `R_{${i}} = \\frac{1}{${q}} \\cdot (${sumExpr})`
    return numerical ? `${prefix} \\; ${z.toTexEq()}` : prefix
  }

  getRExpr2Texs(numerical = false): string[] {
    return this.getRoots().map((z, i) => this.getRExpr2Tex(z, i, numerical));
  }

}


export abstract class SolverMulti extends Solver {

  constructor(n: number, divs: number[], public R: Rational[]) {
    super(n, divs, R.map(r => Complex.theta(r.p, r.q)));
  }

  decompose(q: number): Solver {
    return new SolverMultiChild(this, q);
  }

  get n(): number {
    return this.R.length;
  }

}


export class SolverMultiRoot extends SolverMulti {

  constructor() {
    super(1, [], [new Rational(1)]);
  }

}


function computeMultiChildRoots(parent: SolverMulti, q: number): Rational[] {
  let R = parent.R;
  let R2: Rational[];
  if (parent.getDivs().indexOf(q) === -1) {
    R2 = new Array(R.length * (q - 1));
    for (let i = 0; i < R.length; i++) {
      for (let j = 1; j < q; j++) {
        R2[i * (q - 1) + j - 1] = R[i].sum(new Rational(j, q)).modulo();
      }
    }
  } else {
    R2 = new Array(R.length * q);
    for (let i = 0; i < R.length; i++) {
      for (let j = 0; j < q; j++) {
        R2[i * q + j] = R[i].divN(q).sum(new Rational(j, q)).simplify();
      }
    }
  }
  return R2;
}


export class SolverMultiChild extends SolverMulti {

  constructor(public parent: SolverMulti, q: number) {
    super(
      parent.getSize() * q,
      [...parent.getDivs(), q],
      computeMultiChildRoots(parent, q)
    );
  }

  diviseParent(): boolean {
    return (this.parent.p % this.q) === 0;
  }

}


export class Resolver {

  solver: Solver;
  divs: number[];

  constructor(public p: number) {
    this.divs = computeDivisors(p);
    if (this.divs.length === 1) {
      this.solver = new SolverPrimeRoot(p);
      this.divs = computeDivisors(p - 1);
    } else {
      this.solver = new SolverMultiRoot();
    }
  }

  getDivisors(): number[] {
    return this.divs;
  }

  decompose(q: number): Solver {
    this.divs.splice(this.divs.indexOf(q), 1);
    this.solver = this.solver.decompose(q);
    return this.solver;
  }

}


export class SolverPath {

  prime = false;
  path: number[];
  length: number;
  solvers: Solver[];

  constructor(public p: number) {
    let solverRoot: Solver;
    let divs = computeDivisors(p);
    if (divs.length === 1) {  // p prime
      this.prime = true;
      divs = computeDivisors(p - 1);
      solverRoot = new SolverPrimeRoot(p);
    } else {
      solverRoot = new SolverMultiRoot();
    }
    this.path = [1, ...divs.map((n, i) => divs[divs.length - 1 - i])];
    this.length = this.path.length;
    this.solvers = new Array<Solver>(this.length);
    this.solvers[0] = solverRoot;
    this.computeSolvers();
  }

  setPath(path: number[]) {
    this.path = path;
    this.computeSolvers();
  }

  computeSolvers(from = 1) {
    for (let i = from; i < this.length; i++) {
      this.solvers[i] = this.solvers[i - 1].decompose(this.path[i]);
    }
  }

  getDivAt(i: number): number {
    return this.path[i];
  }

  getDivsAt(i: number): number[] {
    if (i === 0) return [1];
    let divs = this.path.slice(i);
    return divs.filter((n, i) => divs.indexOf(n) === i);
  }

  setDivAt(i: number, q: number): void {
    let slice1 = this.path.slice(0, i);
    let slice2 = this.path.slice(i);
    slice2.splice(slice2.indexOf(q), 1);
    this.path = [...slice1, q, ...slice2];
    this.computeSolvers(i);
  }

  getSolverAt(i: number): Solver {
    return this.solvers[i];
  }

}
