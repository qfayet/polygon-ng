
function isTiny(x: number, y: number = 0): boolean {
  return Math.abs(x - y) < 1e-3;
}

function truncate(x: number, d: number = 3): number {
  let coef = Math.pow(10, d);
  return Math.round(x * coef) / coef;
}

export class Complex {

  constructor(public x: number = 0, public y: number = 0) { }

  opp(): Complex {
    return new Complex(-this.x, -this.y);
  }

  conj(): Complex {
    return new Complex(this.x, -this.y);
  }

  sum(z: Complex): Complex {
    return new Complex(this.x + z.x, this.y + z.y);
  }

  sub(z: Complex): Complex {
    return this.sum(z.opp());
  }

  scale(c: number): Complex {
    return new Complex(this.x * c, this.y * c);
  }

  mult(z: Complex): Complex {
    return new Complex(this.x * z.x - this.y * z.y, this.x * z.y + this.y * z.x);
  }

  dist2(): number {
    return this.x * this.x + this.y * this.y;
  }

  inv(): Complex {
    return this.conj().scale(1 / this.dist2());
  }

  div(z: Complex): Complex {
    return this.mult(z.inv());
  }

  dist(): number {
    return Math.sqrt(this.dist2());
  }

  angle(): number {
    let a = Math.atan2(this.y, this.x);
    if (a < 0) a += 2 * Math.PI;
    return isTiny(a) ? 2 * Math.PI : a;
  }

  pow(n: number): Complex {
    let r = this.dist();
    let a = this.angle();
    return Complex.fromPolar(Math.pow(r, n), a * n);
  }

  truncate(d: number = 3): Complex {
    return new Complex(truncate(this.x, d), truncate(this.y, d));
  }

  toString(): string {
    let z = this.truncate();
    return `(${z.x}, ${z.y})`;
  }

  toStringZ(): string {
    let z = this.truncate();
    if (isTiny(z.y)) return `${z.x}`;
    if (isTiny(z.x)) return z.y < 0 ? `- i x ${Math.abs(z.y)}` : `i x ${z.y}`;
    return z.y < 0 ? `${z.x} - i x ${Math.abs(z.y)}` : `${z.x} + i x ${z.y}`;
  }

  toStringPolar(c = 1): string {
    let r = this.dist();
    let a = this.angle() / (2 * Math.PI);
    if (c === 1) return `(${truncate(r)}, ${truncate(a)})`
    return `(${truncate(r)}, ${truncate(a * c)}/${c})`;
  }

  static fromPolar(r: number, a: number): Complex {
    return new Complex(r * Math.cos(a), r * Math.sin(a));
  }

  static theta(i: number, q: number): Complex {
    return Complex.fromPolar(1, 2 * Math.PI * i / q);
  }

  static compare(z1: Complex, z2: Complex): number {
    let d = z1.angle() - z2.angle();
    return isTiny(d) ? z1.dist() - z2.dist() : d;
  }

  toTex(z: Complex | undefined = undefined): string {
    if (!z) z = this.truncate();
    if (isTiny(z.y)) return `${z.x}`;
    return `\\begin{pmatrix} ${z.x} \\\\ ${z.y} \\end{pmatrix}`;
  }

  equals(z: Complex) {
    return this.x === z.x && this.y === z.y;
  }

  equals2(z: Complex) {
    if (isTiny(this.x)) return this.y === z.y;
    if (isTiny(this.y)) return this.x === z.x;
    return this.x === z.x && this.y === z.y;
  }

  toTexEq(z: Complex | undefined = undefined): string {
    if (!z) z = this.truncate();
    return `${this.equals2(z) ? "=" : "\\approx"} ${this.toTex(z)}`;
  }

}
