
export class Vector {

  static fromCycle(p: number, cycle: number[]): number[] {
    let r = Vector.create(p);
    cycle.forEach(i => r[i] = 1);
    return r;
  }

  static create(w: number): number[] {
    // return Array(w).fill(0);
    let r = new Array<number>(w);
    for (let i = 0; i < w; i++) r[i] = 0;
    return r;
  }

  static clone(v: number[]): number[] {
    return [...v];
  }

  static zero(v: number[]): number[] {
    return Vector.create(v.length);
  }

  static sum(v: number[], v2: number[]): number[] {
    return v.map((x, i) => x + v2[i]);
  }

  static scale(v: number[], n: number): number[] {
    return v.map(x => x * n);
  }

  static rotate(v: number[], n: number): number[] {
    return v.map((x, i) => v[(i + v.length - n) % v.length]);
  }

  static normalize(v: number[]): number[] {
    return v.map(x => x - v[0]);
  }

  static mult(v: number[], v2: number[]): number[] {
    let w = v.length;
    let r = Vector.create(w);
    for (let i = 0; i < w; i++)
      if (v[i] !== 0)
        for (let i2 = 0; i2 < w; i2++)
          r[(i + i2) % w] += v[i] * v2[i2];
    return r;
  }

  static print(v: number[]): void {
    console.log(v);
  }

  static toTex(v: number[]): string {
    let nums = v.map(x => `${x}`);
    return `(${nums.join(", ")})`;
  }

  static toMatRowTex(v: number[]): string {
    let nums = v.map(x => `${x}`);
    return `${nums.join(" & ")}`;
  }

}
