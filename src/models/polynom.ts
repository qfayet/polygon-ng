
export class Polynom {

  static create(deg: number): number[] {
    let r = new Array<number>(deg + 1);
    for (let i = 0; i < r.length; i++) r[i] = 0;
    return r;
  }

  static getDegree(p: number[]): number {
    for (let i = p.length - 1; i >= 0; i--) {
      if (p[i] !== 0) return i;
    }
    return -1;
  }

  static trim(p: number[]): number[] {
    return p.slice(0, Polynom.getDegree(p) + 1);
  }

  static sum(p: number[], p2: number[]): number[] {
    let r = new Array<number>(Math.max(p.length, p2.length));
    for (let i = 0; i < r.length; i++) r[i] = (i < p.length ? p[i] : 0) + (i < p2.length ? p2[i] : 0);
    return Polynom.trim(r);
  }

  static scale(p: number[], n: number): number[] {
    let r = new Array<number>(p.length);
    for (let i = 0; i < r.length; i++) r[i] = p[i] * n;
    return r;
  }

  static shift(p: number[], n: number): number[] {
    let r = new Array<number>(n + p.length);
    for (let i = 0; i < n; i++) r[i] = 0;
    for (let i = 0; i < p.length; i++) r[n + i] = p[i];
    return r;
  }

  static div(p: number[], p2: number[]): {q: number[], r: number[]} {
    let q = Polynom.create(p.length - p2.length);
    let r = p;
    while (r.length >= p2.length) {
      let deg = r.length - p2.length;
      let coef = r[r.length - 1] / p2[p2.length - 1];
      q[deg] = coef;
      r = Polynom.sum(r, Polynom.shift(Polynom.scale(p2, -coef), deg));
    }
    return {q: q, r: r};
  }

  static getPolynomCyclotomic(n: number): number[] {
    let r = Polynom.create(n);
    r[0] = -1;
    r[r.length - 1] = 1;
    for (let i = 1; i < n; i++) {
      if (n % i === 0) r = Polynom.div(r, Polynom.getPolynomCyclotomic(i)).q;
    }
    return r;
  }

  static toTexPart(coef: number, degree: number, start: boolean): string {
    let num = Math.abs(coef);
    if (num === 0) return "";
    let numPart = num === 1 ? degree === 0 ? "1" : "" : `${num}`;
    let xPart = degree === 0 ? "" : degree === 1 ? "X" : `X^{${degree}}`;
    let numAndX = numPart + xPart;
    if (start) return coef > 0 ? numAndX : `-${numAndX}`;
    return coef > 0 ? ` + ${numAndX}` : ` - ${numAndX}`;
  }

  static toTex(p: number[]): string {
    let tex = ""
    for (let i = p.length - 1; i >= 0; i--) {
      tex += Polynom.toTexPart(p[i], i, i === p.length - 1);
    }
    return tex;
  }

}
