import { Vector as Vec, Vector } from "./vector";


export class Matrix {

  static one(m: number[][]): number[][] {
    let r = Matrix.zero(m);
    r[0][0] = 1;
    return r;
  }

  static create(h: number, w: number,): number[][] {
    // return [...new Array(h)].map(x => Vec.new(w));
    let r = new Array<number[]>(h);
    for (let i = 0; i < h; i++) r[i] = Vec.create(w);
    return r;
  }

  static clone(m: number[][]): number[][] {
    return m.map(v => Vec.clone(v));
  }

  static zero(m: number[][]): number[][] {
    return m.map(v => Vec.zero(v));
  }

  static sum(m: number[][], m2: number[][]): number[][] {
    return m.map((v, i) => Vec.sum(v, m2[i]));
  }

  static scale(m: number[][], n: number): number[][] {
    return m.map(v => Vec.scale(v, n));
  }

  static scaleV(m: number[][], v2: number[]): number[][] {
    return m.map(v => Vec.mult(v, v2));
  }

  static rotate(m: number[][], n: number): number[][] {
    return m.map((v, i) => m[(i + m.length - n) % m.length]);
  }

  static normalize(m: number[][], off = 0, filter: number[] | undefined = undefined): number[][] {
    let r = m.map(v => Vec.normalize(v));
    for (let j = 1; j < m[0].length; j++) {
      let d = r[0][j];
      if (filter === undefined || filter[j] === 1) {
        d -= off;
      }
      for (let i = 0; i < m.length; i++) r[i][j] -= d;
    }
    return r;
  }

  static simplify(m: number[][]): number[][] {
    let r = Matrix.normalize(m);
    for (let j = 1; j < m[0].length; j++) {
      let d = r[1][j];
      let same = true;
      for (let i = 2; i < m.length; i++) if (r[i][j] !== d) same = false;
      if (same) for (let i = 0; i < m.length; i++) r[i][j] -= d;
    }
    return r;
  }

  static simplifyCoordsH(v: number[]): void {
    let occus = {};
    let occuMax = 0;
    let occuMaxVal = 0;
    for (let i = 1; i < v.length; i++) {
      let x = v[i];
      let occu = occus[x];
      occu = occu ? occu + 1: 1;
      occus[x] = occu;
      if (occu > occuMax) {
        occuMax = occu;
        occuMaxVal = x;
      }
    }
    if (occuMax > (v.length - 1) / 2) {
      for (let i = 0; i < v.length; i++) {
        v[i] -= occuMaxVal;
      }
    }
  }

  static simplifyCoordsV(m: number[][], j: number): void {
    let occus = {};
    let occuMax = 0;
    let occuMaxVal = 0;
    for (let i = 1; i < m.length; i++) {
      let x = m[i][j];
      let occu = occus[x];
      occu = occu ? occu + 1: 1;
      occus[x] = occu;
      if (occu > occuMax) {
        occuMax = occu;
        occuMaxVal = x;
      }
    }
    if (occuMax > (m.length - 1) / 2) {
      for (let i = 0; i < m.length; i++) {
        m[i][j] -= occuMaxVal;
      }
    }
  }

  static simplifyCoords(m: number[][]): number[][] {
    let r = m.map(v => [0, ...v]);
    for (let j = 1; j < r[0].length; j++) {
      Matrix.simplifyCoordsV(r, j);
    }
    for (let i = 0; i < r.length; i++) {
      Matrix.simplifyCoordsH(r[i]);
    }
    return r;
  }

  static positively(m: number[][]): number[][] {
    let r = Matrix.normalize(m);
    for (let j = 1; j < m[0].length; j++) {
      let min = r[0][j];
      for (let i = 1; i < m.length; i++) if (r[i][j] < min) min = r[i][j];
      for (let i = 0; i < m.length; i++) r[i][j] -= min;
    }
    return r;
  }

  static mult(m: number[][], m2: number[][]): number[][] {
    let h = m.length;
    let w = m[0].length;
    let r = Matrix.create(h, w);
    for (let i = 0; i < h; i++)
      for (let j = 0; j < w; j++)
        if (m[i][j] !== 0)
          for (let i2 = 0; i2 < h; i2++)
            for (let j2 = 0; j2 < w; j2++)
              r[(i + i2) % h][(j + j2) % w] += m[i][j] * m2[i2][j2];
    return r;
  }

  static print(m: number[][]): void {
    m.map(v => Vec.print(v));
  }

  static fromVector(v: number[], i: number, h: number): number[][] {
    let r = Matrix.create(h, v.length);
    r[i % h] = Vec.clone(v);
    return r;
  }

  static toTex(m: number[][], type='p'): string {
    let rows = m.map(v => Vector.toMatRowTex(v));
    return `\\begin{${type}matrix}
    ${rows.join(" \\\\\n")}
    \\end{${type}matrix}`;
  }

}
