
export function computeDivisors(n: number): number[] {
  let divisors = new Array<number>();
  let p = 2;
  while (n > 1) {
    if (n % p === 0) {
      divisors.push(p);
      n /= p;
    } else {
      p++;
    }
  }
  return divisors;
}


export function multDivisors(divs: number[]): number {
  return divs.reduce((total, p) => total * p, 1);
}


export class Rational {

  constructor(public p = 0, public q = 1) { }

  inv(): Rational {
    return new Rational(this.q, this.p);
  }

  opp(): Rational {
    return new Rational(-this.p, this.q);
  }

  sum(r: Rational): Rational {
    return new Rational(this.p * r.q + this.q * r.p, this.q * r.q);
  }

  sumN(n: number): Rational {
    return this.sum(new Rational(n));
  }

  sub(r: Rational): Rational {
    return this.sum(r.opp());
  }

  subN(n: number): Rational {
    return this.sum(new Rational(-n));
  }

  mult(r: Rational): Rational {
    return new Rational(this.p * r.p, this.q * r.q);
  }

  multN(n: number): Rational {
    return new Rational(this.p * n, this.q);
  }

  div(r: Rational): Rational {
    return this.mult(r.inv());
  }

  divN(n: number): Rational {
    return new Rational(this.p, this.q * n);
  }

  simplify(): Rational {
    let divsP = computeDivisors(this.p);
    let divsQ = computeDivisors(this.q);
    let divsP2 = new Array<number>();
    divsP.forEach(p => {
      let i = divsQ.indexOf(p);
      if (i !== -1) {
        divsQ.splice(i, 1);
      } else {
        divsP2.push(p);
      }
    });
    return new Rational(multDivisors(divsP2), multDivisors(divsQ));
  }

  modulo(): Rational {
    let r = this.simplify();
    return new Rational(r.p % r.q, r.q);
  }

  toString(): string {
    return `${this.p}/${this.q}`
  }

}
