
export class Cycle {

  static getCycle(n: number, c: number): number[] {
    let cycle = [];
    for (let i = 1; cycle.indexOf(i) === -1; i = (i * c) % n) cycle.push(i);
    return cycle;
  }

  static findCycle(n: number): number[] {
    for (let i = 1; i < n; i++) {
      let cycle = Cycle.getCycle(n, i);
      if (cycle.length === n - 1 && cycle[cycle.length - 1] !== 0) return cycle;
    }
    return [];
  }

  static decomposeCycle(cycle: number[], q: number): number[][] {
    let p = cycle.length / q;
    let cycles = new Array<number[]>(q);
    for (let i = 0; i < q; i++) cycles[i] = new Array<number>(p);
    for (let i = 0; i < q; i++)
      for (let j = 0; j < p; j++)
        cycles[i][j] = cycle[q * j + i];
    return cycles;
  }

  static toString(cycle: number[]): string {
    return `(${cycle.join(", ")})`;
  }

}

export class Base {

  static create(p: number): number[][] {
    return [Cycle.findCycle(p)];
  }

  static decomposeBase(base: number[][], q: number): number[][] {
    let r = new Array<number[]>(base.length * q);
    for (let i = 0; i < base.length; i++) {
      let cycles = Cycle.decomposeCycle(base[i], q);
      for (let j = 0; j < q; j++) r[q * i + j] = cycles[j];
    }
    return r;
  }

  static toString(base: number[][]): string {
    return `[${base.map(Cycle.toString).join(", ")}]`;
  }

}
