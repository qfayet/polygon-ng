import { Component, OnInit } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { StorageService } from '@app/storage.service';

const DEFAULT_POLY_NUMS = [5, 7, 9, 11, 13, 15, 18];

function getDefaultPolyNum(): number {
  return DEFAULT_POLY_NUMS[Math.floor(Math.random() * DEFAULT_POLY_NUMS.length)];
  // return 13;
}

@Component({
  selector: 'app-resolver-chooser',
  templateUrl: './resolver-chooser.component.html',
  styleUrls: ['./resolver-chooser.component.css']
})
export class ResolverChooserComponent implements OnInit {

  constructor(private router: Router, private storageService: StorageService) {
    this.router.events.subscribe(event => this.onRouteChange(event));
  }

  ngOnInit(): void {
  }

  gotToSolver(): void {
    if (this.storageService.polyNum === undefined) {
      this.storageService.polyNum = getDefaultPolyNum();
    }
    this.router.navigate([`/resolver/${this.storageService.polyNum}`]);
  }

  onRouteChange(event: Event): void {
    if (event instanceof NavigationEnd) {
      let navigationEvent = (event as NavigationEnd);
      if (navigationEvent.url === '/resolver') {
        this.gotToSolver();
      }
    }
  }

}
