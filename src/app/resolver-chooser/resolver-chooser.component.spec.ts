import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolverChooserComponent } from './resolver-chooser.component';

describe('ResolverChooserComponent', () => {
  let component: ResolverChooserComponent;
  let fixture: ComponentFixture<ResolverChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolverChooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolverChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
