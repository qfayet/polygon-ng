import { Component, Input, OnInit } from '@angular/core';
import { Base } from '@models/cycle';
import { SolverPrimeRoot } from '@models/solver';
import { Vector } from '@models/vector';

@Component({
  selector: 'app-solver-prime-root',
  templateUrl: './solver-prime-root.component.html',
  styleUrls: ['./solver-prime-root.component.css']
})
export class SolverPrimeRootComponent implements OnInit {

  @Input()
  solver: SolverPrimeRoot;

  @Input()
  numerical = false;

  showExplanation: false;

  constructor() { }

  ngOnInit(): void {
  }

  getBaseTheta(c: number): string {
    return `\\theta^\\frac{${c}}{${this.solver.p}}`;
  }

  getBaseCycle(): string {
    let thetas = this.solver.base[0].map(c => this.getBaseTheta(c));
    return [...thetas, this.getBaseTheta(1)].join(" \\rightarrow ");
  }

  getGenFunction(): string {
    let g = this.solver.base[0].length > 1 ? this.solver.base[0][1] : 2;
    return `f: z \\mapsto z^{${g}}`;
  }

  getBaseString(): string {
    return Base.toString(this.solver.base);
  }

  getRVec(): string {
    return `${Vector.toTex(this.solver.getR()[0])}`
  }

  getRSum(): string {
    return `\\sum_{1 \\leq i < ${this.solver.getSize()}} \\theta^\\frac{i}{${this.solver.getSize()}}`
  }

  getSolveContent1(): string {
    return `$$
    \\textrm{Donc } \\boxed{R_0 = -1}
    $$`;
  }

  getSolveContent2(): string {
    return `$$
    \\textrm{Car} \\sum_{0 \\leq i < ${this.solver.getSize()}} \\theta^\\frac{i}{${this.solver.getSize()}} = 0
    \\implies \\sum_{1 \\leq i < ${this.solver.getSize()}} \\theta^\\frac{i}{${this.solver.getSize()}} = - \\theta^\\frac{0}{${this.solver.getSize()}} = -1
    $$`;
  }

}
