import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolverPrimeRootComponent } from './solver-prime-root.component';

describe('SolverPrimeRootComponent', () => {
  let component: SolverPrimeRootComponent;
  let fixture: ComponentFixture<SolverPrimeRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolverPrimeRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolverPrimeRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
