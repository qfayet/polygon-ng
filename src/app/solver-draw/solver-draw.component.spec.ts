import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolverDrawComponent } from './solver-draw.component';

describe('SolverDrawComponent', () => {
  let component: SolverDrawComponent;
  let fixture: ComponentFixture<SolverDrawComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolverDrawComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolverDrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
