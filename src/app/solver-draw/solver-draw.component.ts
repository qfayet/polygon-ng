import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Complex } from '@models/complex';
import { Solver } from '@models/solver';

@Component({
  selector: 'app-solver-draw',
  templateUrl: './solver-draw.component.html',
  styleUrls: ['./solver-draw.component.css']
})
export class SolverDrawComponent implements OnInit {

  @Input()
  solver: Solver;

  @ViewChild('canvas')
  canvas: ElementRef;

  @Input()
  size = 500;
  @Input()
  dash = 10;
  @Input()
  margin = 0.5;
  @Input()
  hAxis = false;

  ctx: CanvasRenderingContext2D;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    canvas.width = this.size;
    canvas.height = this.size;
    this.ctx = canvas.getContext('2d');
    this.updateView();
  }

  printDash(cx: number, cy: number) {
    this.ctx.beginPath();
    this.ctx.moveTo(cx - this.dash, cy);
    this.ctx.lineTo(cx + this.dash, cy);
    this.ctx.moveTo(cx, cy - this.dash);
    this.ctx.lineTo(cx, cy + this.dash);
    // let dashDiag = this.dash / Math.sqrt(2);
    // this.cx.moveTo(cx - dashDiag, cy - dashDiag);
    // this.cx.lineTo(cx + dashDiag, cy + dashDiag);
    // this.cx.moveTo(cx - dashDiag, cy + dashDiag);
    // this.cx.lineTo(cx + dashDiag, cy - dashDiag);
    this.ctx.stroke();
  }

  printCross(cx: number, cy: number) {
    this.ctx.beginPath();
    let dashDiag = this.dash / Math.sqrt(2);
    this.ctx.moveTo(cx - dashDiag, cy - dashDiag);
    this.ctx.lineTo(cx + dashDiag, cy + dashDiag);
    this.ctx.moveTo(cx - dashDiag, cy + dashDiag);
    this.ctx.lineTo(cx + dashDiag, cy - dashDiag);
    this.ctx.stroke();
  }

  printRiAt(center: number, ray: number, z: Complex, i: number) {
    let rT = 30;
    let dx = -14;
    let dy = 8;
    let a = z.angle();
    let rTx = rT * Math.cos(a);
    let rTy = - rT * Math.sin(a);
    let xT = center + ray * z.x + rTx + dx;
    let yT = center - ray * z.y + rTy + dy;
    this.ctx.font = '24px serif';
    this.ctx.fillText('R', xT, yT);
    this.ctx.font = '12px serif';
    this.ctx.fillText(`${i}`, xT + 18, yT + 5);
  }

  printTheta(center: number, ray: number, p: number) {
    let rT = -35;
    let dx = -16;
    let dy = 9;
    let z = Complex.theta(1, p);
    let a = z.angle();
    let rTx = rT * Math.cos(a);
    let rTy = - rT * Math.sin(a);
    let xT = center + ray * z.x + rTx + dx;
    let yT = center - ray * z.y + rTy + dy;
    this.ctx.font = '24px serif';
    this.ctx.fillText('θ', xT, yT);
    this.ctx.font = '12px serif';
    this.ctx.fillText(`1/${p}`, xT + 12, yT - 12);
  }

  getCoordMaxZ(z: Complex): number {
    return Math.max(Math.abs(z.x), Math.abs(z.y));
  }

  getCoordMax(): number {
    return this.solver.getRoots().reduce((prev, z) => Math.max(prev, this.getCoordMaxZ(z)), 0);
  }

  updateView() {
    let p = this.solver.p;

    this.ctx.clearRect(0, 0, this.size, this.size);

    let center = this.size / 2;
    let coordMax = Math.max(1.0, this.getCoordMax());
    let ray = this.size / (coordMax * (2 + this.margin));

    this.ctx.strokeStyle = '#000';
    this.ctx.lineWidth = 0.5;
    this.ctx.beginPath();
    this.ctx.arc(center, center, ray, 0, 2 * Math.PI);
    this.ctx.stroke();

    this.ctx.strokeStyle = '#000';
    this.ctx.lineWidth = 0.5;
    if (this.hAxis) {
      this.ctx.beginPath();
      this.ctx.moveTo(0, center);
      this.ctx.lineTo(this.size, center);
      this.ctx.stroke();
    }
    this.ctx.lineWidth = 1.0;
    this.printDash(center, center);

    this.ctx.fillStyle = '#000';
    this.printTheta(center, ray, p);

    this.ctx.strokeStyle = '#F00';
    this.ctx.fillStyle = '#F00';
    this.ctx.lineWidth = 1.5;
    this.ctx.beginPath();
    let z = Complex.theta(p - 1, p);
    let x = center + ray * z.x;
    let y = center + ray * z.y;
    this.ctx.moveTo(x, y);
    for (let i = 0; i < p; i++) {
      let z = Complex.theta(i, p);
      let x = center + ray * z.x;
      let y = center + ray * z.y;
      this.ctx.lineTo(x, y);
    }
    this.ctx.stroke();
    for (let i = 0; i < p; i++) {
      let z = Complex.theta(i, p);
      let x = center + ray * z.x;
      let y = center + ray * z.y;
      this.ctx.beginPath();
      this.ctx.arc(x, y, 3, 0, 2 * Math.PI);
      this.ctx.fill();
    }

    this.ctx.strokeStyle = '#008000';
    this.ctx.fillStyle = '#008000';
    this.ctx.font = '24px serif';
    this.ctx.lineWidth = 2.5;
    this.solver.getRoots().forEach((z, i) => {
      let x = center + ray * z.x;
      let y = center - ray * z.y;
      this.printCross(x, y);
      this.printRiAt(center, ray, z, i);
    });
  }

}
