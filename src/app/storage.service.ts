import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  polyNum: number;
  solverPath: number[];
  stepIndex = 0;

  constructor() { }

  isVerifStep(): boolean {
    return this.solverPath && this.stepIndex === this.solverPath.length;
  }

}
