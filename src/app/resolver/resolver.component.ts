import { StepperSelectionEvent, STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { SolverComponent } from '@app/solver/solver.component';
import { StorageService } from '@app/storage.service';
import { Polynom } from '@models/polynom';
import { Solver, SolverPath } from '@models/solver'

interface StepElement {
  index: number;
  divisor: number;
  choices: number[];
  solver: Solver;
}

@Component({
  selector: 'app-resolver',
  templateUrl: './resolver.component.html',
  styleUrls: ['./resolver.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS,
    useValue: { displayDefaultIndicatorType: false },
  }],
})
export class ResolverComponent implements OnInit {

  degree: number;
  polynom: number[];
  resolver: SolverPath;
  stepElts = new Array<StepElement>();
  numerical = false;
  chipPrefix = "";  // "", "Cas" ou "Technique"

  constructor(private router: Router, private route: ActivatedRoute, public storageService: StorageService) {
    this.route.params.subscribe(params => this.setNum(parseInt(params['num'])));
  }

  ngOnInit(): void {
  }

  setNum(num: number): void {
    if (isNaN(num)) {
      this.router.navigate(["/resolver"]);
      return;
    }
    if (num < 1) {
      this.router.navigate(["/resolver/1"]);
      return;
    }
    if (num > 100) {
      this.router.navigate(["/resolver/100"]);
      return;
    }
    this.degree = num;
    this.polynom = Polynom.getPolynomCyclotomic(num);
    this.resolver = new SolverPath(num);
    if (this.storageService.polyNum === num && this.storageService.solverPath) {
      this.resolver.setPath(this.storageService.solverPath);
    } else {
      this.storageService.polyNum = num;
      this.storageService.stepIndex = 0;
      this.storageService.solverPath = this.resolver.path;
    }
    this.stepElts = new Array<StepElement>(this.resolver.length);
    this.updateStepElts();
  }

  updateStepElts(from = 0) {
    for (let i = from; i < this.stepElts.length; i++) {
      this.stepElts[i] = {
        index: i,
        divisor: this.resolver.getDivAt(i),
        choices: this.resolver.getDivsAt(i),
        solver: this.resolver.getSolverAt(i),
      }
    }
  }

  stepperSelectionChange(event: StepperSelectionEvent) {
    this.storageService.stepIndex = event.selectedIndex;
  }

  divSelectionChange(event: MatSelectChange, stepElt: StepElement) {
    this.resolver.setDivAt(stepElt.index, event.value);
    this.storageService.solverPath = this.resolver.path;
    this.updateStepElts(stepElt.index);
  }

  polynomContent(): string {
    return `$$${Polynom.toTex(this.polynom)} = 0$$`;
  }

  getStepState(stepElt: StepElement): string {
    return stepElt.index === 0 ? 'home' : 'current';
  }

  getStepSolver(solverCpt: SolverComponent, stepElt: StepElement, selectedIndex: number): Solver {
    return solverCpt.solver || stepElt.index === selectedIndex ? stepElt.solver : null;
  }

  getNExprPrimeAt(stepElt: StepElement): string {
    if (stepElt.index === 1) return `n = ${stepElt.solver.n}`;
    let divs = this.stepElts.slice(1, stepElt.index + 1).map(e => `${e.divisor}`);
    return `n = ${divs.join(" \\times ")} = ${stepElt.solver.n}`;
  }

  getNExprMultiAt(stepElt: StepElement): string {
    return `n = ${stepElt.solver.q} \\times ${stepElt.solver.p/stepElt.solver.q} = ${stepElt.solver.p}`;
  }

  getNExprAt(stepElt: StepElement): string {
    return this.resolver.prime ? this.getNExprPrimeAt(stepElt) : this.getNExprMultiAt(stepElt);
  }

  getHeaderAt(stepElt: StepElement): string {
    if (stepElt.index === 0) return `$p = ${this.resolver.p}$, &nbsp; $n = 1$`;
    return `$p = ${this.resolver.p}$, &nbsp; $q = ${stepElt.solver.q}$, &nbsp; $${this.getNExprAt(stepElt)}$`;
  }

  onDegreeChange(event: any) {
    this.updateRoot();
  }

  updateRoot() {
    this.router.navigate([`/resolver/${this.degree}`]);
  }

  decremente() {
    this.degree -= 1;
    this.updateRoot();
  }

  incremente() {
    this.degree += 1;
    this.updateRoot();
  }

  isPrime(): boolean {
    return this.resolver.prime;
  }
}
