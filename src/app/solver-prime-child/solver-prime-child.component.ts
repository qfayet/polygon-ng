import { Component, Input, OnInit } from '@angular/core';
import { Base } from '@models/cycle';
import { SolverPrimeChild } from '@models/solver';

@Component({
  selector: 'app-solver-prime-child',
  templateUrl: './solver-prime-child.component.html',
  styleUrls: ['./solver-prime-child.component.css']
})
export class SolverPrimeChildComponent implements OnInit {

  @Input()
  solver: SolverPrimeChild;

  @Input()
  numerical = false;

  constructor() { }

  ngOnInit(): void {
  }

  getBaseString(): string {
    return Base.toString(this.solver.base);
  }

}
