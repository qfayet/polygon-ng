import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolverPrimeChildComponent } from './solver-prime-child.component';

describe('SolverPrimeChildComponent', () => {
  let component: SolverPrimeChildComponent;
  let fixture: ComponentFixture<SolverPrimeChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolverPrimeChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolverPrimeChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
