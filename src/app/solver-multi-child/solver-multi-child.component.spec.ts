import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolverMultiChildComponent } from './solver-multi-child.component';

describe('SolverMultiChildComponent', () => {
  let component: SolverMultiChildComponent;
  let fixture: ComponentFixture<SolverMultiChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolverMultiChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolverMultiChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
