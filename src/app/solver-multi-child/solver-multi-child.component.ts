import { Component, Input, OnInit } from '@angular/core';
import { Complex } from '@models/complex';
import { Rational } from '@models/rational';
import { SolverMultiChild } from '@models/solver';

@Component({
  selector: 'app-solver-multi-child',
  templateUrl: './solver-multi-child.component.html',
  styleUrls: ['./solver-multi-child.component.css']
})
export class SolverMultiChildComponent implements OnInit {

  @Input()
  solver: SolverMultiChild;

  @Input()
  numerical = false;

  constructor() { }

  ngOnInit(): void {
  }

  getBaseString(): string {
    return this.solver.R.map(r => r.toString()).join(", ");
  }

  getSolveContent1A(): string {
    return `$$
    \\textrm{Comme }${this.solver.q} \\textrm{ divise } ${this.solver.parent.p} \\textrm{ : } \\;
    R_i = {R'}_{\\lfloor \\frac{i}{q} \\rfloor}^{\\frac{1}{q}} \\cdot \\theta^{\\frac{i\\%q}{q}}
    $$`;
  }

  getSolveContent1B(): string {
    return `$$
    \\textrm{Comme }${this.solver.q} \\textrm{ ne divise pas } ${this.solver.parent.p} \\textrm{ : } \\;
    R_i = {R'}_{\\lfloor \\frac{i}{q - 1} \\rfloor} \\cdot \\theta^{\\frac{i\\%(q-1)+1}{q}} \\;
    $$`;
  }

  getSolveContent1(): string {
    return (this.solver.diviseParent()) ? this.getSolveContent1A() : this.getSolveContent1B();
  }

  getRExprPartA(i: number, r: Rational, z: Complex): string {
    let q = this.solver.q;
    let ip = Math.floor(i / q);
    let rp = this.solver.parent.R[ip];
    let exp1 = `{{R'}_{${ip}}}^{\\frac{1}{${q}}} \\cdot \\theta^{\\frac{${i%q}}{${q}}}`
    let exp2 = `{(\\theta^{\\frac{${rp.p}}{${rp.q}}})}^{\\frac{1}{${q}}} \\cdot \\theta^{\\frac{${i%q}}{${q}}}`
    return `${exp1} = ${exp2}`;
  }

  getRExprPartB(i: number, r: Rational, z: Complex): string {
    let q = this.solver.q;
    let ip = Math.floor(i / (q - 1));
    let rp = this.solver.parent.R[ip];
    let exp1 = `{R'}_{${ip}} \\cdot \\theta^{\\frac{${i%(q-1) + 1}}{${q}}}`
    let exp2 = `\\theta^{\\frac{${rp.p}}{${rp.q}}} \\cdot \\theta^{\\frac{${i%(q-1) + 1}}{${q}}}`
    return `${exp1} = ${exp2}`;
  }

  getRExprPart(i: number, r: Rational, z: Complex): string {
    return (this.solver.diviseParent()) ? this.getRExprPartA(i, r, z) : this.getRExprPartB(i, r, z);
  }

  getRExprTex(i: number, r: Rational, z: Complex, numerical = false): string {
    let prefix = `R_{${i}} = ${this.getRExprPart(i, r, z)} = \\theta^{\\frac{${r.p}}{${r.q}}}`;
    return numerical ? `${prefix} \\; ${z.toTexEq()}`: prefix;
  }

  getRExprTexs(numerical = false): string[] {
    return this.solver.R.map((r, i) => this.getRExprTex(i, r, this.solver.getRoots()[i], numerical));
  }

}
