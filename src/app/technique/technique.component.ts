import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technique',
  templateUrl: './technique.component.html',
  styleUrls: ['./technique.component.css']
})
export class TechniqueComponent implements OnInit {

  content1 = `
  La méthode diffère selon que $p$ (le nombre de sommets du polygone) est premier ou pas.<br>
  Nous allons commencé par le cas, relativement plus simple, où $p$ est un multiple.<br>
  Puis, nous aborderons ensuite le cas où $p$ est un nombre premier.`;

  content2 = `
  Si $p$ est un multiple alors $p$ peut s'écrire :
  $$ p = r \\times q \\text{, } \\; \\text{ avec } r > 1 \\text{ et } q \\text{ premier } $$
  `;

  content2A = `
  Si $q$ divise $r$, alors les racines p-ième sont l'ensemble des racines q-ième de l'ensemble des racines r-ième de l'unité :
  $$ R_i = {R'}_{\\lfloor \\frac{i}{q} \\rfloor}^{\\frac{1}{q}} \\cdot \\theta^{\\frac{i\\%q}{q}} $$
  `;

  content2B = `
  Sinon, si $q$ ne divise pas $r$, alors les racines p-ième de l'unité sont l'ensemble des racines r-ième de l'unité combinées avec l'ensemble des racines q-ième de l'unité :
  $$ R_i = {R'}_{\\lfloor \\frac{i}{q - 1} \\rfloor} \\cdot \\theta^{\\frac{i\\%(q-1)+1}{q}} $$
  `;

  content3 = `
  En partant des élements de la base du corps formés de la somme des racines appartenant à chaque sous-cycle :
  $$ R_i = \\sum_{c \\in \\mathcal{C}_i} \\theta^{\\frac{c}{p}} $$
  On définit des $Q_i$ de la sorte :
  $$ Q_i = \\sum_j^q { R_{i \\Delta q + j} \\cdot \\theta^{\\frac{i j}{q}} } $$
  Les $R_i$ peuvent alors se déduire des $Q_i$ par la réciproque :
  $$ R_i = \\frac{1}{q} \\sum_j^q \\frac{Q_{i \\Delta q + j}}{\\theta^{\\frac{i j}{q}}} $$
  Puis, on définit les $P_i$ de la sorte :
  $$ P_i = Q_i \\cdot Q_{n - 1}^{i \\% q} $$
  Les $Q_i$ peuvent donc également se déduire des $P_i$ :
  $$ Q_i = \\frac{P_i}{Q_{n - 1}^{i \\% q}} $$
  L'ensemble des $P_i$ sont constructibles à partir du sous-groupe, et on a :
  $$ P_{n - 1} = Q_{n - 1}^q $$
  On peut donc choisir pour $Q_{n - 1}$ une des racine q-ième de $P_{n - 1}$, que l'on nommera $Q$ :
  $$ Q = Q_{n - 1} = \\sqrt[q]{P_{n - 1}} $$
  Et ainsi, calculer l'ensemble des $R_i$ en substituant les $Q_i$ par leurs expressions sous forme de $P_i$ et de $Q$ :
  $$ R_i = \\frac{1}{q} \\sum_j^q{ \\frac{P_{i \\Delta q + j}}{Q^{j}} \\cdot \\theta^{\\frac{-ij}{q}} } $$
  On itère ainsi de sous-corps en corps parent jusqu'à arriver au corps dont les élements de la base sont les racines p-ièmes de l'unité.
  `;

  constructor() { }

  ngOnInit(): void {
  }

}
