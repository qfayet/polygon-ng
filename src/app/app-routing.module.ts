import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PresentationComponent } from './presentation/presentation.component';
import { ConventionComponent } from './convention/convention.component';
import { TechniqueComponent } from './technique/technique.component';
import { ResolverChooserComponent } from './resolver-chooser/resolver-chooser.component';
import { ResolverComponent } from './resolver/resolver.component';

const routes: Routes = [
  { path: '', redirectTo: 'presentation', pathMatch: 'full' },
  { path: 'presentation', component: PresentationComponent },
  { path: 'convention', component: ConventionComponent },
  { path: 'technique', component: TechniqueComponent },
  {
    path: 'resolver',
    component: ResolverChooserComponent,
    children: [
      {
        path: ':num',
        component: ResolverComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
