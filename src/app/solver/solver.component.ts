import { Component, Input, OnInit } from '@angular/core';
import { Solver, SolverMultiChild, SolverMultiRoot, SolverPrimeChild, SolverPrimeRoot } from '@models/solver';

@Component({
  selector: 'app-solver',
  templateUrl: './solver.component.html',
  styleUrls: ['./solver.component.css']
})
export class SolverComponent implements OnInit {

  @Input()
  solver: Solver;

  @Input()
  numerical = false;

  constructor() { }

  ngOnInit(): void {
  }

  isPrimeRoot(): boolean {
    return this.solver instanceof SolverPrimeRoot;
  }

  asPrimeRoot(): SolverPrimeRoot {
    return this.solver as SolverPrimeRoot;
  }

  isPrimeChild(): boolean {
    return this.solver instanceof SolverPrimeChild;
  }

  asPrimeChild(): SolverPrimeChild {
    return this.solver as SolverPrimeChild;
  }

  isMultiRoot(): boolean {
    return this.solver instanceof SolverMultiRoot;
  }

  asMultiRoot(): SolverMultiRoot {
    return this.solver as SolverMultiRoot;
  }

  isMultiChild(): boolean {
    return this.solver instanceof SolverMultiChild;
  }

  asMultiChild(): SolverMultiChild {
    return this.solver as SolverMultiChild;
  }

}
