import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolverMultiRootComponent } from './solver-multi-root.component';

describe('SolverMultiRootComponent', () => {
  let component: SolverMultiRootComponent;
  let fixture: ComponentFixture<SolverMultiRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolverMultiRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolverMultiRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
