import { Component, Input, OnInit } from '@angular/core';
import { SolverMultiRoot } from '@models/solver';

@Component({
  selector: 'app-solver-multi-root',
  templateUrl: './solver-multi-root.component.html',
  styleUrls: ['./solver-multi-root.component.css']
})
export class SolverMultiRootComponent implements OnInit {

  @Input()
  solver: SolverMultiRoot;

  @Input()
  numerical = false;

  constructor() { }

  ngOnInit(): void {
  }

  getBaseString(): string {
    return this.solver.R.map(r => r.toString()).join(", ");
  }

  getSolveContent(): string {
    return `$$
    \\boxed{R_0 = \\theta^{\\frac{1}{1}} = 1}
    $$`;
  }

}
