import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-convention',
  templateUrl: './convention.component.html',
  styleUrls: ['./convention.component.css']
})
export class ConventionComponent implements OnInit {

  contents = [
    `$a \\% b$ signifiera le reste de la division euclidiene de $a$ par $b$ (comme en informatique)`,
    `$a \\Delta b$ signifiera $b$ muliplié par le quotient de la division euclidiene de $a$ par $b$, c'est-à-dire $\\lfloor \\frac{a}{b} \\rfloor \\cdot b$`,
    `On a donc : $a \\Delta b + a \\% b = a$`,
    `$\\sum_i^n$ représentera une abréviation de $\\sum_{0 \\leq i < n}$ (à la manière des boucles <i>for</i> en informatique)`,
    `$p$ désignera généralement le nombre de sommets du polygone régulier`,
    `$\\theta^{\\frac{1}{p}}$ représentera une des racines de la p-ième équation cyclotomique, et que l'on nommera racine p-ième de l'unité,<br/>
     de telle sorte que l'ensemble des $\\theta^{\\frac{i}{p}}$ avec $0 \\leq i < p$ forment les sommets du polygone régulier<br/>
     (d'une certaine manière, $\\theta$ pourra être considéré comme une abréviation de  $e^{i 2 \\pi}$)`,
    `À noter que l'origine du repère étant le baricentre du polygone régulier, on a (pour $p > 1$) : $$
    \\sum_i^p{ \\theta ^ \\frac{i}{p} } = 0
     $$`,
    `$(a_0, a_1, \\ldots, a_{p-1})$ représentera un vecteur composé des racines p-ièmes de l'unité, tel que :$$
     (a_0, a_1, \\ldots, a_{p-1}) = \\sum_i^p{ a_i \\theta ^ \\frac{i}{p} }
     $$`,
    `De la même manière, une matrice dont chaque ligne représente un vecteur composé des racines p-ièmes de l'unité multiplié par une racime q-ième de l'unité, aura pour signification :$$
     \\begin{pmatrix}
     a_{0, 0}   & a_{0, 1}   & \\ldots & a_{0, p-1}   \\\\
     a_{1, 0}   & a_{1, 1}   & \\ldots & a_{1, p-1}   \\\\
     \\vdots    & \\vdots    &         & \\vdots      \\\\
     a_{q-1, 0} & a_{q-1, 1} & \\ldots & a_{q-1, p-1} \\\\
     \\end{pmatrix} = \\sum_i^q { (a_{i, 0}, a_{i, 1}, \\ldots, a_{i, p-1}) \\cdot \\theta^{\\frac{i}{q}} } = \\sum_i^q { \\sum_j^p {a_{i, j} \\theta^{\\frac{j}{p}} \\theta^{\\frac{i}{q}} } }
     $$`,
     `Une matrice dont les délimiteurs sont des crochets signifiera les cooordonnées d'un point dont une ligne représente un vecteur
     composé des éléments de la base du sous-corps précédent, multiplié par une racime q-ième de l'unité :$$
     \\begin{bmatrix}
     a_{0, 0}   & a_{0, 1}   & \\ldots & a_{0, n'-1}   \\\\
     a_{1, 0}   & a_{1, 1}   & \\ldots & a_{1, n'-1}   \\\\
     \\vdots    & \\vdots    &         & \\vdots      \\\\
     a_{q-1, 0} & a_{q-1, 1} & \\ldots & a_{q-1, n'-1} \\\\
     \\end{bmatrix}
     = \\sum_i^q { (a_{i, 0} R'_{0} + a_{i, 1} R'_{1} + \\ldots + a_{i, n'-1} R'_{n'-1}) \\cdot \\theta^{\\frac{i}{q}} }
     = \\sum_i^q { \\sum_j^{n'} {a_{i, j} R'_{j} \\theta^{\\frac{i}{q}} } }
     $$`,
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
