import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { ConventionComponent } from '@app/convention/convention.component';
import { PresentationComponent } from '@app/presentation/presentation.component';
import { ResolverChooserComponent } from '@app/resolver-chooser/resolver-chooser.component';
import { ResolverComponent } from '@app/resolver/resolver.component';
import { TechniqueComponent } from '@app/technique/technique.component';
import { MathjaxModule } from 'mathjax-angular';
import { SolverMultiChildComponent } from './solver-multi-child/solver-multi-child.component';
import { SolverMultiRootComponent } from './solver-multi-root/solver-multi-root.component';
import { SolverPrimeChildComponent } from './solver-prime-child/solver-prime-child.component';
import { SolverPrimeRootComponent } from './solver-prime-root/solver-prime-root.component';
import { SolverComponent } from './solver/solver.component';
import { SolverCheckerComponent } from './solver-checker/solver-checker.component';
import { MatChipsModule } from '@angular/material/chips';
import { SolverDrawComponent } from './solver-draw/solver-draw.component';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [
    AppComponent,
    ConventionComponent,
    TechniqueComponent,
    PresentationComponent,
    ResolverComponent,
    ResolverChooserComponent,
    SolverComponent,
    SolverPrimeRootComponent,
    SolverPrimeChildComponent,
    SolverMultiRootComponent,
    SolverMultiChildComponent,
    SolverCheckerComponent,
    SolverDrawComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatTabsModule,
    MathjaxModule.forRoot(),
    MatExpansionModule,
    MatChipsModule,
    MatCheckboxModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
