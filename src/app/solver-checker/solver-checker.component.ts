import { Component, Input, OnInit } from '@angular/core';
import { Solver } from '@models/solver';

@Component({
  selector: 'app-solver-checker',
  templateUrl: './solver-checker.component.html',
  styleUrls: ['./solver-checker.component.css']
})
export class SolverCheckerComponent implements OnInit {

  @Input()
  solver: Solver;

  constructor() { }

  ngOnInit(): void {
  }

  getCheckExpression(): string {
    let rMin = this.solver.getRootMin();
    let i = this.solver.getRoots().indexOf(rMin);
    let exprPart = `\\lvert R_{${i}} - \\theta^{\\frac{1}{${this.solver.p}}} \\rvert`;
    let accuracy = this.solver.getAccuracy(rMin);
    if (accuracy > -3) return `${exprPart} \\: > \\: 10^{-3}`;
    let comparPart = isFinite(accuracy) ? `< \\: 10^{${accuracy}}` : `= \\: 0`;
    return `${exprPart} \\: ${comparPart}`;
  }

  isOK(): boolean {
    return this.solver.getAccuracy() <= -3;
  }

}
