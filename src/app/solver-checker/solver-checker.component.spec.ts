import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolverCheckerComponent } from './solver-checker.component';

describe('SolverCheckerComponent', () => {
  let component: SolverCheckerComponent;
  let fixture: ComponentFixture<SolverCheckerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolverCheckerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolverCheckerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
